module WpApiClient
  module Entities
    Types = [
      Image,
      Post,
      Meta,
      Term,
      Taxonomy,
      Error
    ]
  end
end
