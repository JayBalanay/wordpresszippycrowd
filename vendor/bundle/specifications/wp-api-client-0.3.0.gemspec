# -*- encoding: utf-8 -*-
# stub: wp-api-client 0.3.0 ruby lib

Gem::Specification.new do |s|
  s.name = "wp-api-client"
  s.version = "0.3.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.metadata = { "allowed_push_host" => "https://rubygems.org" } if s.respond_to? :metadata=
  s.require_paths = ["lib"]
  s.authors = ["Duncan Brown"]
  s.bindir = "exe"
  s.date = "2016-04-19"
  s.email = ["duncanjbrown@gmail.com"]
  s.homepage = "https://github.com/duncanjbrown/wp-api-client"
  s.licenses = ["MIT"]
  s.required_ruby_version = Gem::Requirement.new("~> 2.3")
  s.rubygems_version = "2.5.1"
  s.summary = "A read-only client for the WordPress REST API (v2)."

  s.installed_by_version = "2.5.1" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<faraday>, ["~> 0.9"])
      s.add_runtime_dependency(%q<faraday_middleware>, ["~> 0.10"])
      s.add_runtime_dependency(%q<faraday-http-cache>, ["~> 1.2"])
      s.add_runtime_dependency(%q<simple_oauth>, ["~> 0.3"])
      s.add_runtime_dependency(%q<typhoeus>, ["~> 1.0"])
      s.add_development_dependency(%q<bundler>, ["~> 1.11"])
      s.add_development_dependency(%q<rake>, ["~> 10.0"])
      s.add_development_dependency(%q<rspec>, ["~> 3.0"])
      s.add_development_dependency(%q<vcr>, ["~> 3"])
    else
      s.add_dependency(%q<faraday>, ["~> 0.9"])
      s.add_dependency(%q<faraday_middleware>, ["~> 0.10"])
      s.add_dependency(%q<faraday-http-cache>, ["~> 1.2"])
      s.add_dependency(%q<simple_oauth>, ["~> 0.3"])
      s.add_dependency(%q<typhoeus>, ["~> 1.0"])
      s.add_dependency(%q<bundler>, ["~> 1.11"])
      s.add_dependency(%q<rake>, ["~> 10.0"])
      s.add_dependency(%q<rspec>, ["~> 3.0"])
      s.add_dependency(%q<vcr>, ["~> 3"])
    end
  else
    s.add_dependency(%q<faraday>, ["~> 0.9"])
    s.add_dependency(%q<faraday_middleware>, ["~> 0.10"])
    s.add_dependency(%q<faraday-http-cache>, ["~> 1.2"])
    s.add_dependency(%q<simple_oauth>, ["~> 0.3"])
    s.add_dependency(%q<typhoeus>, ["~> 1.0"])
    s.add_dependency(%q<bundler>, ["~> 1.11"])
    s.add_dependency(%q<rake>, ["~> 10.0"])
    s.add_dependency(%q<rspec>, ["~> 3.0"])
    s.add_dependency(%q<vcr>, ["~> 3"])
  end
end
