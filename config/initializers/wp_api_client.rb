require 'wp_api_client'

WpApiClient.configure do |api_client|
  api_client.endpoint = ENV['WP_URL']
end
