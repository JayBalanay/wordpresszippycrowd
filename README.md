# Wordpress Zippy Crowd #
This is a sample app that will render the 3 latest blog post of ZippyCrowd.

### How do I get set up? ###

* Clone the repository via
  ```
    git clone git@bitbucket.org:JayBalanay/wordpresszippycrowd.git
  ```
* Setup .env
  ```
    mv .env.sample .env
  ```
  and .ruby-version
  ```
    mv .ruby-version.sample .ruby-version
  ```
* Run the server
  ```
    bundle exec rails s
  ```
