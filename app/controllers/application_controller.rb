class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :initialize_wp_api_client

  protected
  def initialize_wp_api_client
    @wp_api_client = WpApiClient.get_client
  end
end
