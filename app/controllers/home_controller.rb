class HomeController < ApplicationController

  def index
    @posts = @wp_api_client.get('posts/', limit: 3).first(3)
  end
end
