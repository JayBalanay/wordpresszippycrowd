module HomeHelper
  def media_url(media_id)
    @wp_api_client.get("media/#{media_id}").source_url || ''
  end

  def category_description(category_id)
    @wp_api_client.get("categories/#{category_id}").name.html_safe
  end

  def sanitize_excerpt(excerpt)
    excerpt.gsub(/\[\/?et_pb.*?\]/,'').html_safe
  end
end
